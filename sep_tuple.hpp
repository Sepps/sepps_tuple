#pragma once
#include <cstdint>
#include <cstddef>
#include <type_traits>
#include <memory>
#include <initializer_list>
#include <tuple>
namespace sep
{
	template <typename ... Args>
	struct tuple;
};

namespace std
{
	template <typename Tup>
	struct tuple_size;

	template <size_t I, typename Tup>
	struct tuple_element;

	template <typename... Args>
	struct tuple_size<sep::tuple<Args...>> : std::integral_constant<size_t, sizeof...(Args)>{};

	template <typename F, typename ... Args>
	struct tuple_element<0, sep::tuple<F, Args...>>
	{
		using type = F;
	};
	
	template <size_t I, typename F, typename ... Args>
	struct tuple_element<I, sep::tuple<F, Args...>>
	{
		using type = typename tuple_element<I - 1, sep::tuple<Args...>>::type;
	};
}

namespace sep
{
	namespace impl
	{
		struct tuple_no_init{};

		template <typename ...>
		struct TL{};

		enum class RefCat : bool
		{
			L = true,
			R = false,
		};

		template <RefCat ...>
		struct RL{};

		template <size_t ...>
		struct IL{};

		template <size_t count, typename Output = IL<>>
		struct make_il_impl;

		template <size_t count, size_t ... Is>
		struct make_il_impl<count, IL<Is...>>
		{
			using type = typename make_il_impl<count - 1, IL<Is..., sizeof...(Is)>>::type;
		};

		template <size_t ... Is>
		struct make_il_impl<0, IL<Is...>>
		{
			using type = IL<Is...>;
		};

		template <size_t I>
		using make_il = typename make_il_impl<I>::type;

		template <size_t count, typename Output = RL<>>
		struct make_bl_impl;

		template <size_t count, RefCat ... Vs>
		struct make_bl_impl<count, RL<Vs...>>
		{
			using type = typename make_bl_impl<count - 1, RL<Vs..., RefCat::R>>::type;
		};

		template <RefCat ... Vs>
		struct make_bl_impl<0, RL<Vs...>>
		{
			using type = RL<Vs...>;
		};

		template <size_t I>
		using make_bl = typename make_bl_impl<I>::type;

		template <typename T, RefCat Ref>
		using apply_ref = std::conditional_t<Ref == RefCat::L, std::add_lvalue_reference_t<std::add_const_t<T>>, std::add_rvalue_reference_t<T>>;

		template <typename TypeLst, typename RefList>
		struct apply_refs_impl;

		template <typename ... Types, RefCat ... Refs>
			requires (sizeof...(Types) == sizeof...(Refs))
		struct apply_refs_impl<TL<Types...>, RL<Refs...>>
		{
			using type = TL< apply_ref<Types, Refs>... >;
		};

		template <typename TypeList, typename RefList>
		using apply_refs = typename apply_refs_impl<TypeList, RefList>::type;

		template <typename RefList, size_t Index>
		struct ref_at_index_impl;

		template <RefCat f, RefCat ... vs, size_t Index>
		struct ref_at_index_impl<RL<f, vs...>, Index>
		{
			static constexpr RefCat value = ref_at_index_impl<RL<vs...>, Index - 1>::value;
		};

		template <RefCat f, RefCat ... vs>
		struct ref_at_index_impl<RL<f, vs...>, 0>
		{
			static constexpr RefCat value = f;
		};

		template <typename RefList, size_t Index>
		constexpr RefCat ref_at_index = ref_at_index_impl<RefList, Index>::value;

		template <typename RefList, typename IndexList>
		struct extract_indexes_impl;

		template <typename RefList, size_t ... Is>
		struct extract_indexes_impl<RefList, IL<Is...>>
		{
			using type = RL<ref_at_index<RefList, Is>...>;
		};

		template <typename RefList, typename IndexList>
		using ref_extract_indexes = typename extract_indexes_impl<RefList, IndexList>::type;

		template <typename RefList, size_t start, size_t length>
		struct get_ref_sublist_impl;

		template <RefCat f, RefCat ... vs, size_t start, size_t length>
		struct get_ref_sublist_impl<RL<f, vs...>, start, length>
		{
			using type = typename get_ref_sublist_impl<RL<vs...>, start - 1, length>::type;
		};

		template <RefCat ... vs, size_t length>
		struct get_ref_sublist_impl<RL<vs...>, 0, length>
		{
			using type = typename ref_extract_indexes<RL<vs...>, make_il<length>>::type;
		};

		template <typename RefList, size_t start, size_t length>
		using get_ref_sublist = typename get_ref_sublist_impl<RefList, start, length>::type; 

		template <typename RefList, typename Output = RL<>>
		struct reverse_bools_impl;

		template <RefCat ... os>
		struct reverse_bools_impl<RL<>, RL<os...>>
		{
			using type = RL<os...>;
		};

		template <RefCat f, RefCat ... vs, RefCat ... os>
		struct reverse_bools_impl<RL<f, vs...>, RL<os...>>
		{
			using type = typename reverse_bools_impl<RL<vs...>, RL<f, os...>>::type;
		};

		template <typename RefList>
		using reverse_bools = typename reverse_bools_impl<RefList>::type;

		template <RefCat ... vs>
		constexpr size_t CountLeadingTrues(RL<vs...>) noexcept
		{
			size_t output = 0;
			for (auto v : std::initializer_list<RefCat>{ vs... })
			{
				if (static_cast<bool>(v) == true)
				{
					++output;
				}
				else
				{
					break;
				}
			}
			return output;
		}

		template <typename RefList>
		constexpr size_t count_leading_trues = CountLeadingTrues(RefList{});

		template <typename RefList>
		constexpr size_t count_trailing_trues = count_leading_trues<reverse_bools<RefList>>;

		template <typename RefList>
		struct count_bools_impl;

		template <RefCat ... vs>
		struct count_bools_impl<RL<vs...>>
		{
			static constexpr size_t value = sizeof...(vs);
		};

		template <typename RefList>
		constexpr size_t count_bools = count_bools_impl<RefList>::value;

		template <typename RefList>
		struct count_types_impl;

		template <typename ... Ts>
		struct count_types_impl<TL<Ts...>>
		{
			static constexpr size_t value = sizeof...(Ts);
		};

		template <typename TypeList>
		constexpr size_t count_types = count_types_impl<TypeList>::value;

		template <typename RefList, typename IndexList = make_il<count_bools<RefList>>>
		struct inc_ref_list_impl;

		constexpr RefCat get_inc_ref_value(RefCat value, size_t index, size_t pivot)
		{
			if (index == pivot) return static_cast<RefCat>(not static_cast<bool>(value));
			if (index > pivot) return value;
			return static_cast<RefCat>(false);
		}

		template <RefCat ... vs, size_t ... Is>
			requires ( not ( vs and ... ) )
		struct inc_ref_list_impl<RL<vs...>, IL<Is...>>
		{
			static constexpr auto trailing_bools = count_trailing_trues<RL<vs...>>;
			using type = RL<get_inc_ref_value(vs, (sizeof...(Is) - Is - 1), trailing_bools)...>;
		};

		template <RefCat ... vs, size_t ... Is>
			requires ( (sizeof...(vs) == 0) or (vs and ...) )
		struct inc_ref_list_impl<RL<vs...>, IL<Is...>>
		{
			// handle overflow
			using type = RL<static_cast<RefCat>(true), (vs, static_cast<RefCat>(false))...>;
		};

		template <typename RefList>
		using inc_ref_list = typename inc_ref_list_impl<RefList>::type;

		template<typename T>
		struct remove_rvalue_reference
		{
			using type = T;
		};

		template<typename T>
		struct remove_rvalue_reference<T&&>
		{
			using type = T;
		};

		template<typename T>
		using remove_rvalue_reference_t = typename remove_rvalue_reference<T>::type;

		template <typename T>
		struct cvt_ref
		{
			using store_type = T;
			using ref_type = T &;
			using rref_type = T &;
			using cref_type = T const &;

			static constexpr ref_type get_element(ref_type storage) noexcept
			{
				return storage;
			}

			static constexpr cref_type get_element(cref_type storage) noexcept
			{
				return storage;
			}

			template <typename ... Args>
			static constexpr void construct(T & storage, Args && ... args) noexcept(std::is_nothrow_constructible_v<T, Args...>)
			{
				std::construct_at(std::addressof(storage), std::forward<Args>(args)...);
			}

			static constexpr void destroy(T & storage) noexcept(std::is_nothrow_destructible_v<T>)
			{
				std::destroy_at(std::addressof(storage));
			}
		};

		template <typename T>
		struct swap_ref_ptr_impl;

		template <typename T>
		struct swap_ref_ptr_impl<T *>
		{
			using type = T &;
		};

		template <typename T>
		struct swap_ref_ptr_impl<T * const>
		{
			using type = T &;
		};

		template <typename T>
		struct swap_ref_ptr_impl<T &>
		{
			using type = T * const;
		};

		template <typename T>
		struct swap_ref_ptr_impl<T &&>
		{
			using type = T * const;
		};

		template <typename T>
		struct swap_ref_ptr_impl<T const &>
		{
			using type = T const * const;
		};

		template <typename T>
		struct swap_ref_ptr_impl<T const &&>
		{
			using type = T const * const;
		};

		template <typename T>
		using swap_ref_ptr = typename swap_ref_ptr_impl<T>::type;

		template <typename T>
			requires (std::is_reference_v<T>)
		struct cvt_ref<T>
		{
			using store_type = swap_ref_ptr<T>;
			using ref_type = T;
			using const_elem = std::add_const_t<std::remove_reference_t<ref_type>>;
			using cref_type = std::conditional_t
			<
				std::is_lvalue_reference_v<ref_type>,
				std::add_lvalue_reference_t<const_elem>,
				std::add_rvalue_reference_t<const_elem>
			>;

			constexpr static ref_type get_element(store_type & storage) noexcept
			{
				return static_cast<ref_type>(*storage);
			}

			static constexpr void construct(store_type & storage, ref_type data) noexcept
			{
				std::construct_at(&storage, std::addressof(data));
			}

			static constexpr void destroy(store_type & storage) noexcept
			{
				std::destroy_at(&storage);
			}
		};

		template <size_t I, typename T>
			requires (not std::is_void_v<T>)
		struct tuple_item
		{
			using store_man = cvt_ref<remove_rvalue_reference_t<T>>;
			using ref_type = typename store_man::ref_type;
			using store_type = typename store_man::store_type;

			constexpr tuple_item(tuple_no_init) noexcept{}

			constexpr tuple_item(tuple_item && item) noexcept requires(std::is_move_constructible_v<T>)
			{
				initial_ctor(static_cast<std::add_rvalue_reference_t<T>>(item.ref()));
			}

			constexpr tuple_item(tuple_item const & item) noexcept requires(std::is_copy_constructible_v<T>)
			{
				initial_ctor(item.ref());
			}

			constexpr tuple_item & operator=(tuple_item && item) noexcept requires(std::is_move_assignable_v<T>)
			{
				ref() = std::move(item.ref());
				return *this;
			}

			constexpr tuple_item & operator=(tuple_item const & item) noexcept requires(std::is_copy_assignable_v<T>)
			{
				ref() = item.ref();
				return *this;
			}

			constexpr ~tuple_item()
			{
				store_man::destroy(m_value);
			}

			constexpr auto ref() && noexcept -> std::add_rvalue_reference_t<T>
			{
				return static_cast<std::add_rvalue_reference_t<T>>(store_man::get_element(m_value));
			}

			constexpr auto ref() & noexcept -> std::add_lvalue_reference_t<T>
			{
				return store_man::get_element(m_value);
			}

			constexpr auto ref() const & noexcept -> std::add_lvalue_reference_t<std::add_const_t<T>>
			{
				return store_man::get_element(m_value);
			}

			constexpr void default_initial_ctor() noexcept(std::is_nothrow_default_constructible_v<T>) requires (std::is_default_constructible_v<T>)
			{
				store_man::construct(m_value);
			}

			template <typename ... Args>
			constexpr void initial_ctor(Args && ... args) noexcept(std::is_nothrow_constructible_v<T, Args...>) requires (std::is_constructible_v<T, Args...>)
			{
				store_man::construct(m_value, std::forward<Args>(args)...);
			}
		protected:
			union { store_type m_value; };
		};

		template <typename TypeList, size_t I>
		struct at_index_types_impl;

		template <size_t I>
		struct at_index_types_impl<TL<>, I>
		{
			using type = void;
		};

		template <typename F, typename ... Ts, size_t I>
		struct at_index_types_impl<TL<F, Ts...>, I>
		{
			using type = typename at_index_types_impl<TL<Ts...>, I - 1>::type;
		};

		template <typename F, typename ... Ts>
		struct at_index_types_impl<TL<F, Ts...>, 0>
		{
			using type = F;
		};

		template <typename TypeList, size_t I>
		using at_index_types = typename at_index_types_impl<TypeList, I>::type;

		template <typename TypeList, typename Indexes = make_il<count_types<TypeList>>>
		struct tuple_storage;

		template <typename ... Args, size_t ... Is>
		struct tuple_storage<TL<Args...>, IL<Is...>> : tuple_item<Is, Args>...
		{
			constexpr tuple_storage() = delete;
			constexpr tuple_storage(tuple_no_init) noexcept
				: tuple_item<Is, Args>{ tuple_no_init{} }...
			{}

			template <size_t I>
			friend constexpr auto get_item(tuple_storage & storage) noexcept -> tuple_item<I, at_index_types<TL<Args...>, I>>&
			{
				return static_cast<tuple_item<I, at_index_types<TL<Args...>, I>>&>(storage);
			}

			template <size_t I>
			friend constexpr auto get_item(tuple_storage const && storage) noexcept -> tuple_item<I, at_index_types<TL<Args...>, I>> const &&
			{
				return std::move(static_cast<tuple_item<I, at_index_types<TL<Args...>, I>>&>(storage));
			}

			template <size_t I>
			friend constexpr auto get_item(tuple_storage && storage) noexcept -> tuple_item<I, at_index_types<TL<Args...>, I>>&&
			{
				return std::move(static_cast<tuple_item<I, at_index_types<TL<Args...>, I>>&>(storage));
			}

			template <size_t I>
			friend constexpr auto get_item(tuple_storage const & storage) noexcept -> tuple_item<I, at_index_types<TL<Args...>, I>> const &
			{
				return static_cast<tuple_item<I, at_index_types<TL<Args...>, I>> const &>(storage);
			}

			constexpr void default_initial_ctor() noexcept
			{
				(
					get_item<Is>(*this).default_initial_ctor(),
					...
				);
			}

			template <typename ... As>
				requires (sizeof...(As) == sizeof...(Args))
			constexpr void initial_ctor( As && ... as) noexcept
			{
				(
					get_item<Is>(*this).initial_ctor(std::forward<As>(as)),
					...
				);
			}
		};

		enum RefType
		{
			None = 0,
			RValue = 1,
			LValue = 2,
			RValueLValue = RValue | LValue
		};

		template <RefType ... Cs>
		struct CS{};

		template <typename T>
		constexpr auto make_capa() -> RefType
		{
			if (std::is_fundamental_v<T>)
			{
				return RefType::LValue;
			}
			if (std::is_copy_constructible_v<T> and std::is_move_constructible_v<T>)
			{
				return RefType::RValueLValue;
			}
			if (std::is_copy_constructible_v<T>)
			{
				return RefType::LValue;
			}
			if (std::is_move_constructible_v<T>)
			{
				return RefType::RValue;
			}
			return RefType::None;
		}

		template <typename>
		struct get_capa_impl;

		template <typename ... Args>
		struct get_capa_impl<TL<Args...>>
		{
			using type = CS<make_capa<Args>()... >;
		};

		template <typename Ls>
		using get_capa = typename get_capa_impl<Ls>::type;

		template<typename TypeList, typename RefList, typename Indexes, typename RefTypeList>
		struct tuple_impl;

		template <typename Output, RefCat>
		using expand_on_bool = Output;

		template <typename T, RefCat Cat>
		concept constructible_with_ref = std::constructible_from<T, apply_ref<T, Cat>>;

		template <typename T, size_t N>
		struct s_array
		{
			T data[N];
			constexpr size_t size() const noexcept { return N; } 
			constexpr T operator[](size_t i) const noexcept { return data[i]; }
			constexpr T & operator[](size_t i) noexcept { return data[i]; }

			friend constexpr bool operator==(s_array const & lhs, s_array const & rhs) noexcept
			{
				for (size_t i = 0; i < N; ++i) if (lhs[i] != rhs[i]) return false;
				return true;
			}
		};

		template <auto v, typename Is = make_il<v.size()>>
		struct s_array_to_bl_impl;

		template <auto v, size_t ... Is>
		struct s_array_to_bl_impl<v, IL<Is...>>
		{
			using type = RL<v[Is]...>;
		};

		template <auto v>
		using s_array_to_bl = typename s_array_to_bl_impl<v>::type;

		template <typename>
		struct bl_to_s_array_impl;

		template <RefCat ... Vs>
		struct bl_to_s_array_impl<RL<Vs...>>
		{
			static constexpr s_array<RefCat, sizeof...(Vs)> value{ Vs... };
		};

		template <typename T>
		static constexpr auto bl_to_s_array() noexcept { return bl_to_s_array_impl<T>::value; }

		template <typename>
		struct cs_to_s_array_impl;

		template <RefType ... Vs>
		struct cs_to_s_array_impl<CS<Vs...>>
		{
			static constexpr auto value = s_array<RefType, sizeof...(Vs)>{ Vs... };
		};

		template <typename T>
		static constexpr auto cs_to_s_array() noexcept { return cs_to_s_array_impl<T>::value; }

		template <size_t N>
		static constexpr auto first_capa(s_array<RefType, N> const & caps) -> s_array<RefCat, N>
		{
			s_array<RefCat, N> vals{};
			for (size_t i = 0; i < N; ++i) vals[i] = static_cast<RefCat>((caps[i] & RefType::RValue) != RefType::RValue);
			return vals;
		}

		template <size_t N>
		static constexpr auto last_capa(s_array<RefType, N> const & caps) -> s_array<RefCat, N>
		{
			s_array<RefCat, N> vals{};
			for (size_t i = 0; i < N; ++i) vals[i] = static_cast<RefCat>((caps[i] & RefType::LValue) == RefType::LValue);
			return vals;
		}

		template <size_t N>
		struct variable_lut
		{
			s_array<size_t, N> lut;
			size_t length;
		};

		template <size_t N>
		constexpr size_t count_s_trailing_trues(s_array<RefCat, N> const & input, variable_lut<N> const & var) noexcept
		{
			size_t count = 0;
			for (size_t i = 0; i < var.length; ++i)
			{
				auto i_rev = var.length - i - 1;
				if (input[var.lut[i_rev]] == static_cast<RefCat>(true)) ++count;
				else break;
			}
			return count;
		}

		template <size_t N>
		static constexpr bool check_capa(s_array<RefCat, N> const & vals, s_array<RefType, N> const & caps) noexcept
		{
			for (size_t i = 0; i != N; ++i)
			{
				if (vals[i])
				{
					if ((caps[i] & RefType::LValue) != RefType::LValue) return false;
				}
				else
				{
					if ((caps[i] & RefType::RValue) != RefType::RValue) return false;
				}
			}
			return true;
		}

		template <size_t N>
		static constexpr auto variable_bits(s_array<RefType, N> const & caps) noexcept-> variable_lut<N>
		{
			variable_lut<N> out{};
			for (size_t i = 0; i < N; ++i) if (caps[i] == RefType::RValueLValue) out.lut[out.length++] = i;
			return out;
		}

		template <size_t N>
		static constexpr auto next_capa(s_array<RefCat, N> input, s_array<RefType, N> const & caps) noexcept -> s_array<RefCat, N>
		{
			auto var = variable_bits(caps);
			auto index = count_s_trailing_trues<N>(input, var);
			for (size_t i = 0; i < index; ++i)
			{
				auto i_rev = var.length - i - 1;
				input[var.lut[i_rev]] = static_cast<RefCat>(false);
			}
			auto l = var.length - index - 1;
			if (l < var.length) input[var.lut[l]] = static_cast<RefCat>(!static_cast<bool>(input[var.lut[l]]));
			return input;
		}

		template <typename ... Args, RefCat ... Bs, size_t ... Is, RefType ... Cs>
			requires ( ( last_capa(cs_to_s_array<CS<Cs...>>()) == bl_to_s_array<RL<Bs...>>() ) )
		struct tuple_impl<TL<Args...>, RL<Bs...>, IL<Is...>, CS<Cs...>> : tuple_storage<TL<Args...>>
		{
			using storage = tuple_storage<TL<Args...>>;
			using bl = RL<Bs...>;

			constexpr storage & get_storage() noexcept { return *this; }
			constexpr storage const & get_storage() const noexcept { return *this; }

			constexpr tuple_impl(tuple_no_init) noexcept
				: storage{ tuple_no_init{} }
			{}

			static constexpr auto is_nothrow_default_constructible = (std::is_nothrow_default_constructible_v<Args> and ...);

			constexpr tuple_impl() noexcept( is_nothrow_default_constructible ) requires (std::is_default_constructible_v<Args> and ...)
				: storage{ tuple_no_init{} }
			{
				static_cast<storage&>(*this).default_initial_ctor();
			}

			// TODO: this noexcept needs to be calculated
			constexpr tuple_impl(apply_ref<Args, Bs> ... args) noexcept requires( constructible_with_ref<Args, Bs> and ... )
				: storage{ tuple_no_init{} }
			{
				static_cast<storage&>(*this).initial_ctor(static_cast<apply_ref<Args, Bs>>(args)...);
			}
		protected:
			constexpr tuple_impl() = default;
		};

		template <typename RefList, typename RefTypeList>
		using next_tuple_bools = s_array_to_bl<next_capa(bl_to_s_array<RefList>(), cs_to_s_array<RefTypeList>())>;

		template <typename RefTypeList>
		using first_tuple_bools = s_array_to_bl<first_capa(cs_to_s_array<RefTypeList>())>;

		template <typename TypeList, typename RefList, typename IndexList, typename CapaList>
		using next_tuple = tuple_impl<TypeList, next_tuple_bools<RefList, CapaList>, IndexList, CapaList>;

		template <typename TypeList, typename IndexList = make_il<count_types<TypeList>>, typename CapaList = get_capa<TypeList>>
		using first_tuple = tuple_impl<TypeList, first_tuple_bools<CapaList>, IndexList, CapaList>;

		template <typename ... Args, RefCat ... Bs, size_t ... Is, RefType ... Cs>
		struct tuple_impl<TL<Args...>, RL<Bs...>, IL<Is...>, CS<Cs...>>
			: next_tuple<TL<Args...>, RL<Bs...>, IL<Is...>, CS<Cs...>>
		{
			using base = next_tuple<TL<Args...>, RL<Bs...>, IL<Is...>, CS<Cs...>>;
			using storage = tuple_storage<TL<Args...>>;
			using base::base;

			constexpr tuple_impl(apply_ref<Args, Bs> ... args) noexcept requires( constructible_with_ref<Args, Bs> and ... )
				: base{ tuple_no_init{} }
			{
				static_cast<storage&>(*this).initial_ctor(static_cast<apply_ref<Args, Bs>>(args)...);
			}
		};


		template <size_t I = 0>
		struct tuple_compare_impl
		{
			template <typename... Args>
			static constexpr bool less(tuple<Args...> const &lhs, tuple<Args...> const &rhs) noexcept
			{
				if constexpr (I == sizeof...(Args))
				{
					return false;
				}
				else
				{
					if (get<I>(lhs) < get<I>(rhs))
						return true;
					if (get<I>(rhs) < get<I>(lhs))
						return false;
					return tuple_compare_impl<I + 1>::less(lhs, rhs);
				}
			}

			template <typename... Args>
			static constexpr bool greater(tuple<Args...> const &lhs, tuple<Args...> const &rhs) noexcept
			{
				return less(rhs, lhs);
			}

			template <typename... Args>
			static constexpr bool equal(tuple<Args...> const &lhs, tuple<Args...> const &rhs) noexcept
			{
				if constexpr (I == sizeof...(Args))
				{
					return true;
				}
				else
				{
					return (get<I>(lhs) == get<I>(rhs)) and tuple_compare_impl<I + 1>::Equal(lhs, rhs);
				}
			}
		};
	}
}

namespace sep
{
	template <typename ... Args>
	struct tuple : impl::first_tuple<impl::TL<Args...>>
	{
		using self_type= tuple<Args...>;
		using base = impl::first_tuple<impl::TL<Args...>>;
		using base::base;

		constexpr tuple() noexcept(base::is_nothrow_default_constructible) requires((std::is_default_constructible_v<Args> and ...)) = default;
		constexpr tuple(tuple &&) noexcept((std::is_nothrow_move_constructible_v<Args> and ...)) requires((std::is_move_constructible_v<Args> and ...)) = default;
		constexpr tuple(tuple const &) noexcept((std::is_nothrow_copy_constructible_v<Args> and ...)) requires((std::is_copy_constructible_v<Args> and ...)) = default;
		
		constexpr tuple& operator=(tuple const & tup) noexcept(( std::is_nothrow_copy_assignable_v<Args> and ... )) requires((std::is_copy_assignable_v<Args> and ...))
		{
			[this]<size_t ... I>(tuple const & t, impl::IL<I...>)
			{
				((get<I>(*this) = get<I>(t)), ...);
			}(tup, impl::make_il<sizeof...(Args)>{});
			return *this;
		}

		template <typename ... TArgs>
			requires ( sizeof...(Args) == sizeof...(TArgs) and
			requires(Args ... args, std::add_const_t<TArgs> ... targs)
			{
				{((args = targs), ...)};
			})
 		constexpr tuple& operator=(tuple<TArgs...> const & tup) noexcept((std::is_nothrow_assignable_v<Args, TArgs> and ...))
		{
			[&]<size_t ... I>(tuple<TArgs...> const & t, impl::IL<I...>)
			{
				( (get<I>(*this) = get<I>(t) ), ... );
			}(tup, impl::make_il<sizeof...(Args)>{});
			return *this;
		}
		
		constexpr tuple& operator=(tuple && tup) noexcept(( std::is_nothrow_copy_assignable_v<Args> and ... )) requires((std::is_move_assignable_v<Args> and ...))
		{
			[this]<size_t ... I>(tuple const & t, impl::IL<I...>)
			{
				((get<I>(*this) = std::move(get<I>(t))), ...);
			}(tup, impl::make_il<sizeof...(Args)>{});
			return *this;
		}

		template <typename ... TArgs>
			requires ( sizeof...(Args) == sizeof...(TArgs) and
			requires(Args ... args, TArgs && ... targs)
			{
				{((args = std::move(targs)), ...)};
			})
 		constexpr tuple& operator=(tuple<TArgs...> && tup) noexcept((std::is_nothrow_assignable_v<Args, TArgs&&> and ...))
		{
			[&]<size_t ... I>(tuple<TArgs...> const & t, impl::IL<I...>)
			{
				( (get<I>(*this) = std::move(get<I>(t)) ), ... );
			}(tup, impl::make_il<sizeof...(Args)>{});
			return *this;
		}

		constexpr bool operator<(tuple const &rhs) const noexcept
		{
			return impl::tuple_compare_impl<>::less(*this, rhs);
		}

		constexpr bool operator>(tuple const &rhs) const noexcept
		{
			return impl::tuple_compare_impl<>::greater(*this, rhs);
		}

		constexpr bool operator<=(tuple const &rhs) const noexcept
		{
			auto &lhs = *this;
			return not(lhs > rhs);
		}

		constexpr bool operator>=(tuple const &rhs) const noexcept
		{
			auto &lhs = *this;
			return not(lhs < rhs);
		}

		constexpr bool operator==(tuple const &rhs) const noexcept
		{
			return impl::tuple_compare_impl<>::equal(*this, rhs);
		}

		constexpr bool operator!=(tuple const &rhs) const noexcept
		{
			return not operator==(rhs);
		}
	};

	template <typename>
	struct is_sep_tuple_impl
	{
		static constexpr auto value = false;
	};

	template <typename ... Args>
	struct is_sep_tuple_impl<sep::tuple<Args...>>
	{
		static constexpr auto value = true;
	};

	template <typename T>
	constexpr bool is_sep_tuple = is_sep_tuple_impl<T>::value;

	template <typename T>
	concept SepTuple = is_sep_tuple<T>;

	template <size_t I, typename ... Args>
	constexpr auto get(tuple<Args...> const & tup) noexcept -> std::tuple_element_t<I, tuple<Args...>> const &
	{
		return get_item<I>(tup.get_storage()).ref();
	}

	template <size_t I, typename ... Args>
	constexpr auto get(tuple<Args...> & tup) noexcept -> std::tuple_element_t<I, tuple<Args...>> &
	{
		return get_item<I>(tup.get_storage()).ref();
	}

	template <size_t I, typename ... Args>
	constexpr auto get(tuple<Args...> const && tup) noexcept -> std::tuple_element_t<I, tuple<Args...>> const &&
	{
		return get_item<I>(tup.get_storage()).ref();
	}

	template <size_t I, typename ... Args>
	constexpr auto get(tuple<Args...> && tup) noexcept -> std::add_rvalue_reference_t<std::tuple_element_t<I, tuple<Args...>>>
	{
		return std::move(get_item<I>(tup.get_storage())).ref();
	}

	// Allow swapping normal tuples
	template <typename ... Args>
		requires (not ((std::is_reference_v<Args> and ...) and not (std::is_const_v<Args> and ...)))
	constexpr void swap(tuple<Args...> & lhs, tuple<Args...> & rhs) noexcept
	{
		auto seq = impl::make_il<sizeof...(Args)>{};
		[]<size_t ... I>(auto & lhs, auto & rhs, impl::IL<I...>)
		{
			using std::swap;
			(swap( get<I>(lhs), get<I>(rhs) ), ...);
		}(lhs, rhs, seq);
	}

	// Allow swapping proxy reference tuples
	template <typename ... Args>
		requires ((std::is_reference_v<Args> and ...) and not (std::is_const_v<Args> and ...))
	constexpr void swap(tuple<Args...> lhs, tuple<Args...> rhs) noexcept
	{
		auto seq = impl::make_il<sizeof...(Args)>{};
		[]<size_t ... I>(auto & lhs, auto & rhs, impl::IL<I...>)
		{
			using std::swap;
			(swap( get<I>(lhs), get<I>(rhs) ), ...);
		}(lhs, rhs, seq);
	}

	template <typename ... As>
	constexpr auto forward_as_tuple(As && ... args) noexcept -> tuple<As&&...>
	{
		return { std::forward<As>(args)... };
	}

	template <typename ... As>
	constexpr auto tie(As & ... args) noexcept -> tuple<std::add_lvalue_reference_t<As>...>
	{
		return { args... };
	}

	template <size_t I, size_t C, size_t O, typename Tup>
	struct tuple_lookup_impl;

	template <size_t C, typename Tup>
	constexpr size_t tuple_lookup_elem_size = std::tuple_size_v
	<
		std::decay_t<std::tuple_element_t<C, Tup>>
	>;

	template <size_t I, size_t C, size_t O, typename Tup>
		requires ((I - O) < tuple_lookup_elem_size<C, Tup>)
	struct tuple_lookup_impl<I, C, O, Tup>
	{
		using tuple_type = Tup;
		using tuple_elem = std::tuple_element_t<C, tuple_type>;
		using elem_type = std::tuple_element_t<I - O, std::decay_t<tuple_elem>>;

		static constexpr auto & invoke(tuple_type && tuple) noexcept
		{
			return get<I-O>(get<C>(tuple));
		}
		static constexpr auto & invoke(tuple_type const & tuple) noexcept
		{
			return get<I-O>(get<C>(tuple));
		}
	};

	template <size_t I, size_t C, size_t O, typename Tup>
		requires (not ((I - O) < tuple_lookup_elem_size<C, Tup>))
	struct tuple_lookup_impl<I, C, O, Tup> : tuple_lookup_impl<I, C + 1, O + tuple_lookup_elem_size<C, Tup>, Tup>
	{
		using base = tuple_lookup_impl<I, C + 1, O + tuple_lookup_elem_size<C, Tup>, Tup>;
		using base::invoke;
		using typename base::elem_type;
	};

	template <size_t I, SepTuple Tup>
	constexpr auto tuple_lookup(Tup & tup) noexcept -> typename tuple_lookup_impl<I, 0, 0, Tup>::elem_type
	{
		return tuple_lookup_impl<I, 0, 0, Tup>::invoke(tup);
	}

	template <size_t I, SepTuple Tup>
	constexpr auto tuple_lookup(Tup const & tup) noexcept -> typename tuple_lookup_impl<I, 0, 0, Tup>::elem_type
	{
		return tuple_lookup_impl<I, 0, 0, Tup>::invoke(tup);
	}

	template <size_t ... Is, SepTuple Tup>
	constexpr auto tuple_cat_scatter(impl::IL<Is...>, Tup & tuple) -> sep::tuple<typename tuple_lookup_impl<Is, 0, 0, Tup>::elem_type...>
	{
		return { tuple_lookup<Is>(tuple) ... };
	}

	template <size_t ... Is, SepTuple Tup>
	constexpr auto tuple_cat_scatter(impl::IL<Is...>, Tup const & tuple) -> sep::tuple<typename tuple_lookup_impl<Is, 0, 0, Tup>::elem_type...>
	{
		return { tuple_lookup<Is>(tuple) ... };
	}

	template <SepTuple ... Tups>
	constexpr auto tuple_cat(Tups && ... tuples) 
	{
		constexpr auto total_size = (std::tuple_size_v<std::decay_t<Tups>> + ...);
		return tuple_cat_scatter(impl::make_il<total_size>{}, sep::tuple(std::forward<Tups>(tuples)...));
	}

	template <typename ... Args>
	tuple(Args...)->tuple<Args...>;
}